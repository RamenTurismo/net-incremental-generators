﻿namespace RamenTurismo.Generators.Debug;

[ToString]
public partial class Car
{
    public string? Plate { get; init; }

    public string? Brand { get; init; }
}
