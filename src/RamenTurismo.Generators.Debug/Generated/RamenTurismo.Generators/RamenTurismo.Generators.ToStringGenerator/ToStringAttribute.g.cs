﻿namespace RamenTurismo.Generators
{
    /// <summary>
    /// Automatically generates the <c>ToString</c> method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class ToStringAttribute : Attribute
    {
    }
}