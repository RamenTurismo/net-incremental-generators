﻿namespace RamenTurismo.Generators.Debug;

public partial class Car 
{
    /// <summary>
    /// Auto-generated ToString() method (2 properties).
    /// </summary>
    public override string ToString()
    {
        return $"{nameof(Plate)}={Plate}, {nameof(Brand)}={Brand}";
    }
}