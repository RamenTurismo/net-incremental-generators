﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;

namespace RamenTurismo.Generators;

internal static class RoslynExtensions
{
    public static bool HasKind(this SyntaxTokenList tokenList, SyntaxKind kind)
    {
        if (tokenList.Count == 0)
        {
            return false;
        }

        foreach (SyntaxToken modifier in tokenList)
        {
            if (modifier.IsKind(kind))
            {
                return true;
            }
        }

        return false;
    }
}
