﻿using System.Collections.Immutable;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace RamenTurismo.Generators;

[Generator(LanguageNames.CSharp)]
public sealed class ToStringGenerator : IIncrementalGenerator
{
    private const string ToStringAttributeFullName = "RamenTurismo.Generators.ToStringAttribute";

    private const string ToStringAttribute = @"namespace RamenTurismo.Generators
{
    /// <summary>
    /// Automatically generates the <c>ToString</c> method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class ToStringAttribute : Attribute
    {
    }
}";

    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        // Generate attribute.
        context.RegisterPostInitializationOutput(e => e.AddSource("ToStringAttribute.g.cs", ToStringAttribute));

        // Generate ToString override.
        IncrementalValuesProvider<TypeDeclarationSyntax> syntaxProvider = context.SyntaxProvider
            .CreateSyntaxProvider(
                predicate: IsSyntaxValid,
                transform: GetDeclarationSyntaxes)
            .Where(m => m is not null)!;

        context.RegisterSourceOutput(syntaxProvider.Collect(), GenerateSource);
    }

    internal static void GenerateSource(SourceProductionContext spc, ImmutableArray<TypeDeclarationSyntax> source)
    {
        foreach (TypeDeclarationSyntax item in source)
        {
            spc.CancellationToken.ThrowIfCancellationRequested();

            // TODO think about the inner classes.
            if(item.Parent is not BaseNamespaceDeclarationSyntax namespaceDeclarationSyntax)
            {
                continue;
            }

            string classname = item.Identifier.ToString();
            string namespaceName = namespaceDeclarationSyntax.Name.ToString();

            string[] properties = item.ChildNodes()
                .OfType<PropertyDeclarationSyntax>()
                .Select(e =>
                {
                    string propertyName = e.Identifier.ToString();
                    return $"{{nameof({propertyName})}}={{{propertyName}}}";
                })
                .ToArray();

            spc.AddSource(
                $"{classname}.ToString.g.cs",
                SourceText.From(@$"namespace {namespaceName};

{item.Modifiers} {item.Keyword} {classname} 
{{
    /// <summary>
    /// Auto-generated ToString() method ({properties.Length} properties).
    /// </summary>
    public override string ToString()
    {{
        return $""{string.Join(", ", properties)}"";
    }}
}}", encoding: Encoding.UTF8));
        }
    }

    private static bool IsSyntaxValid(SyntaxNode node, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();

        return node is TypeDeclarationSyntax typeDeclaration
            // To detect the ToStringAttribute later on.
            && typeDeclaration.AttributeLists.Count > 0
            // partial keyword necessary to override ToString.
            && typeDeclaration.Modifiers.HasKind(SyntaxKind.PartialKeyword);
    }

    private static TypeDeclarationSyntax? GetDeclarationSyntaxes(GeneratorSyntaxContext context, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();

        TypeDeclarationSyntax declarationSyntax = (TypeDeclarationSyntax)context.Node;

        foreach (AttributeSyntax attributeSyntax in declarationSyntax.AttributeLists.SelectMany(e => e.Attributes))
        {
            cancellationToken.ThrowIfCancellationRequested();

            if (context.SemanticModel.GetSymbolInfo(attributeSyntax).Symbol is not IMethodSymbol attributeSymbol)
            {
                continue;
            }

            INamedTypeSymbol attributeContainingTypeSymbol = attributeSymbol.ContainingType;
            string fullName = attributeContainingTypeSymbol.ToDisplayString();

            if (string.Equals(ToStringAttributeFullName, fullName, StringComparison.Ordinal))
            {
                return declarationSyntax;
            }
        }

        return null;
    }
}
