﻿namespace RamenTurismo.Generators.Benchmarks;

public class ModifierLookupBenchmark
{
    private SyntaxTokenList _modifiers;

    [GlobalSetup]
    public void Setup()
    {
        _modifiers = new SyntaxTokenList(
            Token(SyntaxKind.PublicKeyword),
            Token(SyntaxKind.PartialKeyword));
    }

    [Benchmark]
    public bool WithForeach()
    {
        foreach (SyntaxToken modifier in _modifiers)
        {
            if (modifier.IsKind(SyntaxKind.PartialKeyword))
            {
                return true;
            }
        }

        return false;
    }

    [Benchmark]
    public bool WithAny()
    {
        return _modifiers.Any(e => e.IsKind(SyntaxKind.PartialKeyword));
    }

    [Benchmark]
    public bool WithContainsToFullString()
    {
        return _modifiers.ToFullString().Contains("partial", StringComparison.OrdinalIgnoreCase);
    }

    [Benchmark]
    public bool WithContainsToString()
    {
        return _modifiers.ToString().ToString().Contains("partial", StringComparison.OrdinalIgnoreCase);
    }
}
