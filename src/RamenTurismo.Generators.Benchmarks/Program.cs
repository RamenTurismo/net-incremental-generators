﻿using System.Reflection;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Mathematics;
using BenchmarkDotNet.Order;
using BenchmarkDotNet.Running;
using RamenTurismo.Generators.Benchmarks;

IConfig config = ManualConfig.CreateMinimumViable()
    .AddDiagnoser(
        new MemoryDiagnoser(new MemoryDiagnoserConfig())
    // new DisassemblyDiagnoser(new DisassemblyDiagnoserConfig(printSource: true))
    )
    .AddExporter(
        MarkdownExporter.GitHub)
    .AddColumn(
        new RankColumn(NumeralSystem.Arabic),
        StatisticColumn.Min,
        StatisticColumn.Max)
    .WithOrderer(new DefaultOrderer(SummaryOrderPolicy.FastestToSlowest));

BenchmarkSwitcher.FromAssembly(Assembly.GetExecutingAssembly()).Run(args, config);

//BenchmarkRunner.Run(typeof(ToStringGeneratorBenchmark), new DebugInProcessConfig(), args);
