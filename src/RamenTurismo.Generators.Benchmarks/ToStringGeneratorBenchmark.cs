﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace RamenTurismo.Generators.Benchmarks;

public class ToStringGeneratorBenchmark
{
    private ImmutableArray<TypeDeclarationSyntax> _declarations;

    [Params(1, 10, 100, 1000)]
    public int Elements { get; set; }

    [GlobalSetup]
    public void Setup()
    {
        List<BaseNamespaceDeclarationSyntax> namespaceDeclarationSyntaxes = new();

        for (int i = 0; i < Elements; i++)
        {
            FileScopedNamespaceDeclarationSyntax namespaceDeclaration = FileScopedNamespaceDeclaration(ParseName("Benchmarks.Tests"));
            TypeDeclarationSyntax declaration = TypeDeclaration(SyntaxKind.ClassDeclaration, "Car");

            declaration = declaration.AddMembers(
                PropertyDeclaration(ParseTypeName("int"), "Id"),
                PropertyDeclaration(ParseTypeName("string"), "Name"),
                PropertyDeclaration(ParseTypeName("string"), "Comment"));

            declaration = declaration.AddAttributeLists(
                AttributeList(
                    SeparatedList(new[] { Attribute(ParseName("ToString")) })));

            namespaceDeclarationSyntaxes.Add(namespaceDeclaration.AddMembers(declaration));
        }

        ImmutableArray<TypeDeclarationSyntax>.Builder builder = ImmutableArray.CreateBuilder<TypeDeclarationSyntax>();
        builder.AddRange(namespaceDeclarationSyntaxes.SelectMany(e => e.ChildNodes()).OfType<TypeDeclarationSyntax>());
        _declarations = builder.ToImmutable();
    }

    [Benchmark]
    public void GenerateSource()
    {
        ToStringGenerator.GenerateSource(new SourceProductionContext(), _declarations);
    }
}
