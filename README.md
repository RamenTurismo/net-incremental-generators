# README

Rewrites the `.ToString()` method of a `class` or `struct` as you code.

## Usage

On a class : 

```csharp
[ToString]
internal partial class Car
{
    public string? Plate { get; set; }

    public string? Brand { get; set; }
}
```

will generate :

```csharp
internal partial class Car
{
    public override string ToString()
    {
        return $"{nameof(Plate)}={Plate}, {nameof(Brand)}={Brand}"
    }
}
```

## Seeing files generated

You need to add the following in your `.csproj` file in a `PropertyGroup` :

```xml
<EmitCompilerGeneratedFiles>true</EmitCompilerGeneratedFiles>
<CompilerGeneratedFilesOutputPath>Generated</CompilerGeneratedFilesOutputPath>
```

To avoid taking these files as compiled in the project, add this lines : 

```xml
<ItemGroup>
    <!-- Exclude the output of source generators from the compilation -->
    <Compile Remove="$(CompilerGeneratedFilesOutputPath)/**/*.cs" />
</ItemGroup>
```

## Documentation used

- [dotnet-how-to-debug-source-generator-vs2022](https://github.com/JoanComasFdz/dotnet-how-to-debug-source-generator-vs2022)